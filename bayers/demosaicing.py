# import pygame module in this program 
import pygame 
import numpy as N
import pygame.surfarray as surfarray
# activate the pygame library . 
# initiate pygame and give permission 
# to use pygame's functionality. 
pygame.init() 
# define the RGB value 
# for white colour 
white = (255, 255, 255) 

# create a surface object, image is drawn on it. 
image = pygame.image.load('7TkXX.png') 

# create the display surface object 
# of specific dimension..e(X, Y). 
display_surface = pygame.display.set_mode(image.get_size()) 

# set the pygame window name 
pygame.display.set_caption('Image') 

debayerarray = surfarray.array3d(image)

bayerarray = debayerarray

for r in range(len(bayerarray)):
    for c in range(len(bayerarray[r])):
        red = int(bayerarray[r,c,0])
        blue = int(bayerarray[r,c,2])
        green = int(bayerarray[r,c,1])
        if r % 2 == 0 and c % 2 == 1:
            try:
                green+=debayerarray[r-1,c-1,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r-1,c+1,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r+1,c-1,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r+1,c+1,1]
            except IndexError:
                pass

            try:
                red+=debayerarray[r+1,c,0]
            except IndexError:
                pass

            try:
                red+=debayerarray[r-1,c,0]
            except IndexError:
                pass

            try:
                blue+=debayerarray[r,c-1,2]
            except IndexError:
                pass

            try:
                blue+=debayerarray[r,c+1,2]
            except IndexError:
                pass
            
            red/=2
            green/=5
            blue/=2    
        elif r % 2 == 0 and c % 2 == 0:
            try:
                red+=debayerarray[r-1,c-1,0]
            except IndexError:
                pass

            try:
                red+=debayerarray[r-1,c+1,0]
            except IndexError:
                pass

            try:
                red+=debayerarray[r+1,c-1,0]
            except IndexError:
                pass

            try:
                red+=debayerarray[r+1,c+1,0]
            except IndexError:
                pass

            try:
                green+=debayerarray[r+1,c,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r-1,c,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r,c-1,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r,c+1,1]
            except IndexError:
                pass
            
            red/=4
            green/=4
            blue/=1
        elif r % 2 == 1 and c % 2 == 0:
            try:
                green+=debayerarray[r-1,c-1,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r-1,c+1,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r+1,c-1,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r+1,c+1,1]
            except IndexError:
                pass

            try:
                blue+=debayerarray[r+1,c,2]
            except IndexError:
                pass

            try:
                blue+=debayerarray[r-1,c,2]
            except IndexError:
                pass

            try:
                red+=debayerarray[r,c-1,0]
            except IndexError:
                pass

            try:
                red+=debayerarray[r,c+1,0]
            except IndexError:
                pass
            
            red/=2
            green/=5
            blue/=2
        elif r % 2 == 1 and c % 2 == 1:
            try:
                blue+=debayerarray[r-1,c-1,2]
            except IndexError:
                pass

            try:
                blue+=debayerarray[r-1,c+1,2]
            except IndexError:
                pass

            try:
                blue+=debayerarray[r+1,c-1,2]
            except IndexError:
                pass

            try:
                blue+=debayerarray[r+1,c+1,2]
            except IndexError:
                pass

            try:
                green+=debayerarray[r+1,c,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r-1,c,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r,c-1,1]
            except IndexError:
                pass

            try:
                green+=debayerarray[r,c+1,1]
            except IndexError:
                pass
            
            red/=1
            green/=4
            blue/=4

        bayerarray[r,c,0] = red
        bayerarray[r,c,2] = blue
        bayerarray[r,c,1] = green

# infinite loop 
while True : 

	# completely fill the surface object 
	# with white colour 
	display_surface.fill(white) 

	# copying the image surface object 
	# to the display surface object at 
	# (0, 0) coordinate. 
    
	surfarray.blit_array(display_surface,bayerarray)

	# iterate over the list of Event objects 
	# that was returned by pygame.event.get() method. 
	for event in pygame.event.get() : 

		# if event object type is QUIT 
		# then quitting the pygame 
		# and program both. 
		if event.type == pygame.QUIT : 

			# deactivates the pygame library 
			pygame.quit() 

			# quit the program. 
			quit() 

		# Draws the surface object to the screen. 
		pygame.display.update() 
			
