# drawCircleArcExample.py     P. Conrad for CS5nm, 10/31/2008
#  How to draw an arc in Pygame that is part of a circle

import pygame
from pygame.locals import *
import sys

import math

pygame.init()
screen = pygame.display.set_mode((900,900))

def drawCircleArc(screen,color,center,radius,startDeg,endDeg,thickness):
    (x,y) = center
    rect = (x-radius,y-radius,radius*2,radius*2)
    startRad = startDeg/180.0 * math.pi
    endRad = endDeg/180.0 * math.pi
   
    pygame.draw.arc(screen,color,rect,startRad,endRad,thickness)
    

white = (255,255,255)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
black = (0,0,0)
start = 0

while True:

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit() 
            sys.exit()


    screen.fill(white)

    drawCircleArc(screen,black,(200,300),125,start,start+180,4)

    if start <= 360:
        start+=1
    else:
        start = 0
    pygame.display.update()