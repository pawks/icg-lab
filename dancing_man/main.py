import pygame 

display_surface = pygame.display.set_mode((500,700))

pygame.init()  
white = (255, 255, 255)
black = (0, 0, 0)


r = 20
x0 = 300
z0 = 200
yb = z0+r
yh = z0-0.5*r
rb = x0+r
lb = x0-r
val = 0

def drawMan(val,screen):
    col1 = (255-val,255-val,255-val)
    col2 = (val,val,val)

    pygame.draw.circle(screen,black,(x0,z0),r,2)
    pygame.draw.line(screen,black,(x0,yb),(x0,yb+70),2)

    pygame.draw.line(screen,col1,(x0,yh+50),(lb,yh+30),1)
    pygame.draw.line(screen,col1,(x0,yh+50),(rb,yh+1.5*30),1)
    pygame.draw.line(screen,col1,(x0,yb+70),(lb,yb+90),1)
    pygame.draw.line(screen,col1,(x0,yb+70),(rb,yb+60),1)

    pygame.draw.line(screen,col2,(x0,yh+50),(lb,yh+1.5*30),1)
    pygame.draw.line(screen,col2,(x0,yh+50),(rb,yh+30),1)
    pygame.draw.line(screen,col2,(x0,yb+70),(lb,yb+60),1)
    pygame.draw.line(screen,col2,(x0,yb+70),(rb,yb+90),1)
    pygame.time.delay(500)

while True : 
    display_surface.fill(white)  
    for event in pygame.event.get() : 
    	if event.type == pygame.QUIT: 
            pygame.quit()  
            quit() 
    drawMan(val,display_surface)
    if val == 0:
        val = 255
    else:
        val = 0

    pygame.display.update()