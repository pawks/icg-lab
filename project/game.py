import pygame
import random
import math
import argparse
import operator
from pygame.locals import *


class SortingHelpFormatter(argparse.HelpFormatter):
    def add_arguments(self, actions):
        actions = sorted(actions, key=operator.attrgetter('option_strings'))
        super(SortingHelpFormatter, self).add_arguments(actions)


def text_format(message, textFont, textSize, textColor):
    newFont = pygame.font.Font(textFont, textSize)
    newText = newFont.render(message, 0, textColor)

    return newText


parser = argparse.ArgumentParser(
    formatter_class=SortingHelpFormatter,
    prog="Arcade Game",
    description="This is a 4 lane castle defence arcade game.")

parser.add_argument('--hero', type=int, help='Select Hero')

parser.add_argument('--badguy', type=int, help='Select Badguy')

args = parser.parse_args()

badguy = [
    'resources/images/badguy.png', 'resources/images/badguy2.png',
    'resources/images/badguy3.png', 'resources/images/badguy4.png'
]
hero = [
    'resources/images/dude.png', 'resources/images/dude2.png',
    'resources/images/tank.png', 'resources/images/spacecraft.png'
]
weapon = [
    'resources/images/bullet.png', 'resources/images/bullet.png',
    'resources/images/missile.png', 'resources/images/lazer.png'
]

if args.hero:
    if args.hero < 4:
        player = pygame.image.load(hero[args.hero])
        arrow = pygame.image.load(weapon[args.hero])
    else:
        exit()

if args.badguy:
    if args.badguy < 4:
        badguyimg1 = pygame.image.load(badguy[args.badguy])
    else:
        exit()

# Variables
pygame.init()
width, height = 640, 480
screen = pygame.display.set_mode((width, height))
white = (255, 255, 255)
black = (0, 0, 0)
gray = (50, 50, 50)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
yellow = (255, 255, 0)
font = 'freesansbold.ttf'
menu = True
selected = "start"

grass = pygame.image.load("resources/images/grass.png")

while menu and not args.hero and not args.badguy:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                selected = "start"
            elif event.key == pygame.K_DOWN:
                selected = "quit"
            if event.key == pygame.K_RETURN:
                if selected == "start":
                    print("Start")
                    menu = False
                if selected == "quit":
                    pygame.quit()
                    quit()

    # Main Menu UI
    screen.fill(0)
    for x in range(int(width / grass.get_width()) + 1):
        for y in range(int(height / grass.get_height()) + 1):
            screen.blit(grass, (x * 100, y * 100))
    title = text_format("CASTLE DEFENCE", font, 60, yellow)
    if selected == "start":
        text_start = text_format("START", font, 40, white)
    else:
        text_start = text_format("START", font, 40, black)
    if selected == "quit":
        text_quit = text_format("QUIT", font, 40, white)
    else:
        text_quit = text_format("QUIT", font, 40, black)

    title_rect = title.get_rect()
    start_rect = text_start.get_rect()
    quit_rect = text_quit.get_rect()

    # Main Menu Text
    screen.blit(title, (width / 2 - (title_rect[2] / 2), 80))
    screen.blit(text_start, (width / 2 - (start_rect[2] / 2), 300))
    screen.blit(text_quit, (width / 2 - (quit_rect[2] / 2), 360))
    pygame.display.update()

menu = True
selected = "DUDE"
while menu and not args.hero and not args.badguy:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                if selected == "DUDE":
                    selected = "SPACECRAFT"
                elif selected == "DUDE-2":
                    selected = "DUDE"
                elif selected == "TANK":
                    selected = "DUDE-2"
                elif selected == "SPACECRAFT":
                    selected = "TANK"
            elif event.key == pygame.K_DOWN:
                if selected == "DUDE":
                    selected = "DUDE-2"
                elif selected == "DUDE-2":
                    selected = "TANK"
                elif selected == "TANK":
                    selected = "SPACECRAFT"
                elif selected == "SPACECRAFT":
                    selected = "DUDE"
            if event.key == pygame.K_RETURN:
                if selected == "DUDE":
                    player = pygame.image.load(hero[0])
                    arrow = pygame.image.load(weapon[0])
                if selected == "DUDE-2":
                    player = pygame.image.load(hero[1])
                    arrow = pygame.image.load(weapon[1])
                if selected == "TANK":
                    player = pygame.image.load(hero[2])
                    arrow = pygame.image.load(weapon[2])
                if selected == "SPACECRAFT":
                    player = pygame.image.load(hero[3])
                    arrow = pygame.image.load(weapon[3])
                menu = False

    # Main Menu UI
    screen.fill(0)
    for x in range(int(width / grass.get_width()) + 1):
        for y in range(int(height / grass.get_height()) + 1):
            screen.blit(grass, (x * 100, y * 100))
    title = text_format("HERO", font, 60, yellow)
    if selected == "DUDE":
        text_dude = text_format("DUDE", font, 40, white)
    else:
        text_dude = text_format("DUDE", font, 40, black)
    if selected == "DUDE-2":
        text_dude2 = text_format("DUDE-2", font, 40, white)
    else:
        text_dude2 = text_format("DUDE-2", font, 40, black)
    if selected == "TANK":
        text_tank = text_format("TANK", font, 40, white)
    else:
        text_tank = text_format("TANK", font, 40, black)
    if selected == "SPACECRAFT":
        text_spacecraft = text_format("SPACECRAFT", font, 40, white)
    else:
        text_spacecraft = text_format("SPACECRAFT", font, 40, black)

    title_rect = title.get_rect()
    dude_rect = text_dude.get_rect()
    dude2_rect = text_dude2.get_rect()
    tank_rect = text_tank.get_rect()
    spacecraft_rect = text_spacecraft.get_rect()

    # Main Menu Text
    screen.blit(title, (width / 2 - (title_rect[2] / 2), 60))
    screen.blit(text_dude, (width / 2 - (dude_rect[2] / 2), 240))
    screen.blit(text_dude2, (width / 2 - (dude2_rect[2] / 2), 300))
    screen.blit(text_tank, (width / 2 - (tank_rect[2] / 2), 360))
    screen.blit(text_spacecraft, (width / 2 - (spacecraft_rect[2] / 2), 420))

    pygame.display.update()

menu = True
selected = "BADGUY-1"
while menu and not args.hero and not args.badguy:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                if selected == "BADGUY-1":
                    selected = "BADGUY-4"
                elif selected == "BADGUY-2":
                    selected = "BADGUY-1"
                elif selected == "BADGUY-3":
                    selected = "BADGUY-2"
                elif selected == "BADGUY-4":
                    selected = "BADGUY-3"
            elif event.key == pygame.K_DOWN:
                if selected == "BADGUY-1":
                    selected = "BADGUY-2"
                elif selected == "BADGUY-2":
                    selected = "BADGUY-3"
                elif selected == "BADGUY-3":
                    selected = "BADGUY-4"
                elif selected == "BADGUY-4":
                    selected = "BADGUY-1"
            if event.key == pygame.K_RETURN:
                if selected == "BADGUY-1":
                    badguyimg1 = pygame.image.load(badguy[0])
                if selected == "BADGUY-2":
                    badguyimg1 = pygame.image.load(badguy[1])
                if selected == "BADGUY-3":
                    badguyimg1 = pygame.image.load(badguy[2])
                if selected == "BADGUY-4":
                    badguyimg1 = pygame.image.load(badguy[3])
                menu = False

    # Main Menu UI
    screen.fill(0)
    for x in range(int(width / grass.get_width()) + 1):
        for y in range(int(height / grass.get_height()) + 1):
            screen.blit(grass, (x * 100, y * 100))
    title = text_format("BAD GUY", font, 60, yellow)
    if selected == "BADGUY-1":
        text_dude = text_format("BADGUY-1", font, 40, white)
    else:
        text_dude = text_format("BADGUY-1", font, 40, black)
    if selected == "BADGUY-2":
        text_dude2 = text_format("BADGUY-2", font, 40, white)
    else:
        text_dude2 = text_format("BADGUY-2", font, 40, black)
    if selected == "BADGUY-3":
        text_tank = text_format("BADGUY-3", font, 40, white)
    else:
        text_tank = text_format("BADGUY-3", font, 40, black)
    if selected == "BADGUY-4":
        text_spacecraft = text_format("BADGUY-4", font, 40, white)
    else:
        text_spacecraft = text_format("BADGUY-4", font, 40, black)

    title_rect = title.get_rect()
    dude_rect = text_dude.get_rect()
    dude2_rect = text_dude2.get_rect()
    tank_rect = text_tank.get_rect()
    spacecraft_rect = text_spacecraft.get_rect()

    # Main Menu Text
    screen.blit(title, (width / 2 - (title_rect[2] / 2), 60))
    screen.blit(text_dude, (width / 2 - (dude_rect[2] / 2), 240))
    screen.blit(text_dude2, (width / 2 - (dude2_rect[2] / 2), 300))
    screen.blit(text_tank, (width / 2 - (tank_rect[2] / 2), 360))
    screen.blit(text_spacecraft, (width / 2 - (spacecraft_rect[2] / 2), 420))

    pygame.display.update()

pygame.mixer.init()

acc = [0, 0]
arrows = []
badtimer = 100
badtimer1 = 0
badguys = [[640, 100]]
healthvalue = 194
keys = [False, False, False, False]
playerpos = [100, 100]
ltime = 90000
speed = [1, 2, 3, 5, 8]
level = 0
delay = False
bscore = 0

# Load Images
castle = pygame.image.load("resources/images/castle.png")
badguyimg = badguyimg1
healthbar = pygame.image.load("resources/images/healthbar.png")
health = pygame.image.load("resources/images/health.png")
gameover = pygame.image.load("resources/images/gameover.png")
youwin = pygame.image.load("resources/images/youwin.png")

# Load audio
hit = pygame.mixer.Sound("resources/audio/explode.wav")
enemy = pygame.mixer.Sound("resources/audio/enemy.wav")
shoot = pygame.mixer.Sound("resources/audio/shoot.wav")
hit.set_volume(0.05)
enemy.set_volume(0.05)
shoot.set_volume(0.05)
pygame.mixer.music.load('resources/audio/moonlight.wav')
pygame.mixer.music.play(-1, 0.0)
pygame.mixer.music.set_volume(0.25)

# 4 - keep looping through
nstart = pygame.time.get_ticks()
running = 1
exitcode = 0
while running:
    badtimer -= 1
    # Calculate Position
    if keys[0] and playerpos[1] > 10:
        playerpos[1] -= 5
    elif keys[2] and playerpos[1] < 470:
        playerpos[1] += 5
    if keys[1] and playerpos[0] > 10:
        playerpos[0] -= 5
    elif keys[3] and playerpos[0] < 630:
        playerpos[0] += 5

    # Tile Grass all over
    screen.fill(0)
    for x in range(int(width / grass.get_width()) + 1):
        for y in range(int(height / grass.get_height()) + 1):
            screen.blit(grass, (x * 100, y * 100))

    # Draw castles
    screen.blit(castle, (0, 30))
    screen.blit(castle, (0, 135))
    screen.blit(castle, (0, 240))
    screen.blit(castle, (0, 345))

    # Draw Player and Castle
    position = pygame.mouse.get_pos()
    angle = math.atan2(position[1] - (playerpos[1] + 32),
                       position[0] - (playerpos[0] + 26))
    playerrot = pygame.transform.rotate(player, 360 - angle * 57.29)
    playerpos1 = (playerpos[0] - playerrot.get_rect().width / 2,
                  playerpos[1] - playerrot.get_rect().height / 2)
    screen.blit(playerrot, playerpos1)

    # Draw HUD
    survivedtext = text_format(
        "Killed:" + str(bscore) + " Level:" + str(level + 1) + "  Time:" + str(
            (nstart + ltime - pygame.time.get_ticks()) / 1000).zfill(2), font,
        24, black)
    textRect = survivedtext.get_rect()
    textRect.topright = [635, 5]
    screen.blit(survivedtext, textRect)
    screen.blit(healthbar, (5, 5))
    for health1 in range(healthvalue):
        screen.blit(health, (health1 + 8, 8))

    if delay:
        pygame.time.delay(1000)
        delay = False

    # Animate Arrows
    for bullet in arrows:
        index = 0
        velx = math.cos(bullet[0]) * 10
        vely = math.sin(bullet[0]) * 10
        bullet[1] += velx
        bullet[2] += vely
        if bullet[1] < -64 or bullet[1] > 640 or bullet[2] < -64 or bullet[
                2] > 480:
            arrows.pop(index)
        index += 1
        for projectile in arrows:
            arrow1 = pygame.transform.rotate(arrow,
                                             360 - projectile[0] * 57.29)
            screen.blit(arrow1, (projectile[1], projectile[2]))

    # Animate Badgers
    if badtimer == 0:
        badguys.append([640, random.randint(50, 430)])
        badtimer = 100 - (badtimer1 * 2)
        if badtimer1 >= 35:
            badtimer1 = 35
        else:
            badtimer1 += 5
    index = 0
    for badguy in badguys:
        if badguy[0] < -64:
            badguys.pop(index)
        badguy[0] -= speed[level]
        badrect = pygame.Rect(badguyimg.get_rect())
        badrect.top = badguy[1]
        badrect.left = badguy[0]
        if badrect.left < 64:
            hit.play()
            healthvalue -= random.randint(5, 20)
            badguys.pop(index)
        index1 = 0
        for bullet in arrows:
            bullrect = pygame.Rect(arrow.get_rect())
            bullrect.left = bullet[1]
            bullrect.top = bullet[2]
            if badrect.colliderect(bullrect):
                bscore += 1
                enemy.play()
                acc[0] += 1
                badguys.pop(index)
                arrows.pop(index1)
            index1 += 1
        index += 1
    for badguy in badguys:
        screen.blit(badguyimg, badguy)

    # Handle Events
    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit(0)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                keys[0] = True
            elif event.key == pygame.K_a:
                keys[1] = True
            elif event.key == pygame.K_s:
                keys[2] = True
            elif event.key == pygame.K_d:
                keys[3] = True
            elif event.key == pygame.K_SPACE:
                shoot.play()
                position = pygame.mouse.get_pos()
                acc[1] += 1
                arrows.append([
                    math.atan2(position[1] - (playerpos1[1] + 32),
                               position[0] - (playerpos1[0] + 26)),
                    playerpos1[0] + 32, playerpos1[1] + 32
                ])
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_w:
                keys[0] = False
            elif event.key == pygame.K_a:
                keys[1] = False
            elif event.key == pygame.K_s:
                keys[2] = False
            elif event.key == pygame.K_d:
                keys[3] = False

    # Win/Lose check
    if pygame.time.get_ticks() - nstart >= ltime:
        if level == 4:
            running = 0
            exitcode = 1
        else:
            if level == 2:
                ltime = 60000
            healthvalue = 194
            level += 1
            nstart = pygame.time.get_ticks()
            arrows = []
            badtimer = 100
            badtimer1 = 0
            badguys = [[640, 100]]
            delay = True

    if healthvalue <= 0:
        running = 0
        exitcode = 0
    if acc[1] != 0:
        accuracy = acc[0] * 1.0 / acc[1] * 100
    else:
        accuracy = 0

# Win/lose display
if exitcode == 0:
    pygame.font.init()
    text = text_format(
        "Killed:" + str(bscore) + " Accuracy: " + str(accuracy) +
        "%\nTotal score:" + str(bscore * accuracy / 100), font, 24, red)
    textRect = text.get_rect()
    textRect.centerx = screen.get_rect().centerx
    textRect.centery = screen.get_rect().centery + 24
    screen.blit(gameover, (0, 0))
    screen.blit(text, textRect)
else:
    pygame.font.init()
    font = pygame.font.Font(None, 24)
    text = font.render(
        "Killed:" + str(bscore) + " Accuracy: " + str(accuracy) +
        "%\nTotal score:" + str(bscore * accuracy / 100), font, 24, red)
    textRect = text.get_rect()
    textRect.centerx = screen.get_rect().centerx
    textRect.centery = screen.get_rect().centery + 24
    screen.blit(youwin, (0, 0))
    screen.blit(text, textRect)
while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit(0)
    pygame.display.flip()
