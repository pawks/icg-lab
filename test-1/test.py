import sys, pygame
pygame.init()

size = width, height = 1920, 1080

screen = pygame.display.set_mode(size)

y = 600
x = 800
separation = 200
inc = 10
size = 50
while True:
    separation = 200
    x=800
    while separation > 0:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
        screen.fill((0,0,0))
        pygame.draw.line(screen,(0,255,0),(x,y),(x+size,y),3)
        pygame.draw.line(screen,(0,255,0),(x+size,y),(x+size-10,y-10),2)
        pygame.draw.line(screen,(0,255,0),(x+size,y),(x+size-10,y+10),2)
        pygame.draw.line(screen,(0,255,0),(x+size+separation,y),(x+2*size+separation,y),3)
        pygame.draw.line(screen,(0,255,0),(x+size+separation,y),(x+size+separation+10,y-10),2)
        pygame.draw.line(screen,(0,255,0),(x+size+separation,y),(x+size+separation+10,y+10),2)
        pygame.display.update()
        pygame.time.delay(500)
        separation-=inc
        x+=inc/2