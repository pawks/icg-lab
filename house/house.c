#include<stdio.h>
#include<graphics.h>
#include "../dda/dda.h"

int main() 
{ 
    int gd = DETECT, gm; 
  
    // Initialize graphics function   
    initgraph (&gd, &gm, "");
    //Main
    ddaline(50,200,100,200,RED);
    ddaline(50,200,50,100,RED);
    ddaline(100,200,100,100,RED);
    ddaline(50,100,100,100,RED);
    ddaline(50,100,75,50,RED);
    ddaline(100,100,75,50,RED);
    ddaline(65,85,85,85,RED);
    ddaline(65,85,65,75,RED);
    ddaline(85,85,85,75,RED);
    ddaline(65,75,85,75,RED);
    ddaline(100,200,170,180,RED);
    ddaline(170,180,170,80,RED);
    ddaline(170,80,145,30,RED);
    ddaline(100,100,170,80,RED);
    ddaline(75,50,145,30,RED);
    ddaline(70,200,70,170,RED);
    ddaline(80,200,80,170,RED);
    ddaline(70,170,80,170,RED);
    getchar();
    closegraph();
    return 0; 
}
