#include<stdio.h>
#include<graphics.h>
#include "bresenham.h"

int main() 
{ 
    int gd = DETECT, gm; 
  
    // Initialize graphics function   
    int x0,y0,r;
    scanf("%d %d %d",&x0,&y0,&r);
    initgraph (&gd, &gm, "");
    bresenhamcircle(x0,y0,r,RED);
    getchar();
    closegraph();
    return 0; 
}
