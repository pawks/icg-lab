#ifndef _BRESENHAM
#define _BRESENHAM
#include <math.h>
void plotLineHigh(int x0,int y0,int x1,int y1,int color)
{
     int dx,dy,xi,D,x,y;
     dx = x1 - x0;
     dy = y1 - y0;
     xi = 1;
     if (dy < 0)
     {
         xi = -1;
         dx = -dx;
     }
     D = 2*dx - dy;
     x = x0;
     for(y=y0;y<=y1;++y)
     {
         putpixel(x,y,color);
         if (D > 0)
         {
             x = x + xi;
             D = D - 2*dy;
         }
         D = D + 2*dx;
     }
}   
void plotLineLow(int x0,int y0,int x1,int y1,int color)
{
    int dx,dy,yi,D,x,y;
    dx = x1 - x0;
    dy = y1 - y0;
    yi = 1;
    if (dy < 0)
    {
        yi = -1;
        dy = -dy;
    }
    D = 2*dy - dx;
    y = y0;
    for(x=x0;x<=x1;++x)
    {
        putpixel(x,y,color);
        if (D > 0)
        {
            y = y + yi;
            D = D - 2*dx;
        }
        D = D + 2*dy;
    }
}
void bresenhamline(int x0,int y0,int x1,int y1,int color)
{
    if (abs(y1 - y0) < abs(x1 - x0))
        if (x0 > x1)
            plotLineLow(x1, y1, x0, y0, color);
        else
            plotLineLow(x0, y0, x1, y1, color);
    else
        if (y0 > y1)
            plotLineHigh(x1, y1, x0, y0, color);
        else
            plotLineHigh(x0, y0, x1, y1, color);
}
void bresenhamcircle(int p,int q,int r,int color)
{
    int x=0,y=r,d;
    d=3-2*r;
    putpixel(p,q,color);
    while(x<y)
    {
        putpixel(x+p, y+q, color);
        putpixel(y+p, x+q, color);
        putpixel(-y+p, x+q, color);
        putpixel(-x+p, y+q, color);
        putpixel(-x+p, -y+q, color);
        putpixel(-y+p, -x+q, color);
        putpixel(y+p, -x+q, color);
        putpixel(x+p, -y+q, color);
        if(d>=0)
        {
            d+=4-4*y;
            y=y-1;
        }
        d+=4*x+6;
        x=x+1;
    }
}
#endif
