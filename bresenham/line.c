#include<stdio.h>
#include <graphics.h>
#include "bresenham.h"

int main() 
{ 
    int gd = DETECT, gm; 
  
    // Initialize graphics function   
    int x0,y0,x1,y1;
    scanf("%d %d %d %d",&x0,&y0,&x1,&y1);
    initgraph (&gd, &gm, "");
    bresenhamline(x0,y0,x1,y1,RED);
    getchar();
    closegraph();
    return 0; 
}  
