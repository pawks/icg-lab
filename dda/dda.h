#ifndef _DDA
#define _DDA
#include<math.h>
int abs (int n) 
{ 
    return ( (n>0) ? n : ( n * (-1))); 
}

void ddaline(int x0,int y0,int x1,int y1,int color)
{
    float dx = x1 - x0; 
    float dy = y1 - y0;
    float slope = (dx)? dy/dx : 0;
    int t1 = abs(dx) > abs(dy) ? x0 : y0;
    int f = abs(dx) > abs(dy) ? x1 : y1;
    float var = abs(dx) > abs(dy) ? y0 : x0;
    int inc = t1 > f ? -1 : 1;
    float step = abs(dx) > abs(dy) ? (float)inc*slope : (slope)?(float)inc/slope:0;
    int inv = abs(dx) > abs(dy) ? 1 : 0;
    //printf("%f,%d,%d,%f,%d,%d,%f\n",slope,t1,f,var,inv,inc,step);
    int i,itr=abs(t1-f);
    for(i=0;i<=itr;i++)
    {
        //printf("%d\t%d\n",t1,(int)round(var));
        if(inv)
            putpixel(t1,(int)round(var),color);
        else
        {
            putpixel((int)round(var),t1,color);
        }
        var+=step;
        if(t1-f!=0)
            t1=t1+inc;
        else
            continue;
    }
}

void ddacircle(int x0,int y0,int r,int color)
{
    int i,j;
    putpixel(x0,y0,color);
    for(i=x0-r;i<=x0+r;++i)
        if(i>0)
            for(j=y0-r;j<=y0+r;++j)
                if((j>0)&&(((int)sqrt(pow(i-x0,2)+pow(j-y0,2)))==r))
                    putpixel(i,j,color);
}

#endif
